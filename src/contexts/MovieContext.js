import Axios from "axios";
import React, { useState, useEffect, createContext } from "react";

const API_URL = "http://backendexample.sanbercloud.com/api/movies";
const INITIAL_STATE_INPUT = { title: "", genre: "", description: "", image_url: "", rating: 0, duration: 120, year: 2020, id: null };

export const MovieContext = createContext();

export const MovieProvider = props => {

    const [data, setData] =  useState([])
    const [input, setInput] = useState(INITIAL_STATE_INPUT)

    useEffect( () => {
          Axios.get(API_URL)
            .then(({ data }) => {
              console.log({ data });
              setData(data);
          })
      }, [])
  
  useEffect(() => {
    console.log("updated");
  }, [data])

    const handleInput = (e) =>{
      const { name, value, valueAsNumber } = e.target;
      setInput(prevInput => {
        return {
          ...prevInput,
          [name]: valueAsNumber || value
        }
      })

    }
  
  const handleEdit = (e) => {
    const { value } = e.target;
    setInput(data.find(({ id }) => id === parseInt(value)));
      console.log(data.find(({ id }) => id === parseInt(value)), value)
  }
  const handleDelete = (e) => {
    const { value } = e.target;
    Axios.delete(`${API_URL}/${value}`).then(() => {
      console.log("deleted")
    })
    setData(prevState => [...prevState.filter(movie => movie.id !== parseInt(value))])
  }

  const handleSubmit = (e) => {
    e.preventDefault()

    if (input.id === null){        
      Axios.post(`${API_URL}`, {...input})
        .then(({ data }) => {
          console.log({ data }, "submitted");
          setData(prevState =>[
            ...prevState, 
            {
              id: data.id,
              ...input
            }])
      });
    } else {
      const { id, ...dataInput } = input;
      Axios.put(`${API_URL}/${id}`, {...dataInput})
      .then(({data: resData}) => {
        console.log("updated", data)
          setData(prevState => {
            let data = [...prevState];
            let indexOfMovie = data.findIndex(movie => movie.id === id);
            data[indexOfMovie] = {
              ...data[indexOfMovie],
              ...resData
            }
            return data;
          })
      })
    }

    setInput(INITIAL_STATE_INPUT)


  }
  console.log({ dataFromX: data });
    return (
      <MovieContext.Provider value={{ data, setData, input, handleInput, handleEdit, handleDelete, handleSubmit }}>
        {props.children}
      </MovieContext.Provider>
    );
  };
