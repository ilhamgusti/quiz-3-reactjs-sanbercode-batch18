import React, { useState, createContext } from "react";

const INITIAL_STATE_LOGIN_INPUT = {
    username: '',
    password:'',
}
export const AuthContext = createContext();

export const AuthProvider = props => {
    const [login, setLogin] = useState({
        username: "admin",
        password:'admin'
    })
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    
    const [input, setInput] = useState(INITIAL_STATE_LOGIN_INPUT)
    
    const handleSubmitLogin = (e) => {
        e.preventDefault();
        setInput(INITIAL_STATE_LOGIN_INPUT);
        if (input.username === login.username && input.password === login.password) {
            setIsLoggedIn(true)
        } else {
            setIsLoggedIn(false);
            
        }
        console.log(input.username === login.username && input.password === login.password, input, login)
        return ;
    }

    const handleInput = (e) =>{
      const { name, value } = e.target;
      setInput(prevInput => {
        return {
          ...prevInput,
          [name]: value
        }
      })

    }
    console.log({ inputLogin:input });
    return (
      <AuthContext.Provider value={{login, handleSubmitLogin, input, handleInput,isLoggedIn, setIsLoggedIn}}>
        {props.children}
      </AuthContext.Provider>
    );
  };
