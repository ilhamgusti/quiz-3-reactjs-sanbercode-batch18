import React from 'react';
import { AuthProvider, MovieProvider } from './contexts';
import router from './routers';

function App() {
  return (
    <div className="App">
      <AuthProvider>
        <MovieProvider>
          {router}
        </MovieProvider>
      </AuthProvider>
    </div>
  );
}

export default App;
