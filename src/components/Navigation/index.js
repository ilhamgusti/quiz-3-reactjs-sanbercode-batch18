import React, { useContext } from 'react'
import { Link } from 'react-router-dom';
import Logo from '../../assets/logo.png';
import { AuthContext } from '../../contexts';
import { publicRouteData, privateRouteData } from '../../routers/routeData';

const Navigation = () => {
    const { isLoggedIn, setIsLoggedIn } = useContext(AuthContext);
    return (
        <header>
            <img id="logo" src={Logo} width="200px" alt="logo Sanber COde" />
            <nav>
                <ul>
                    {
                        isLoggedIn ? <>{privateRouteData.map(({ name, path, id }) => <li key={id}><Link to={path}>{name}</Link></li>)} <li onClick={()=>setIsLoggedIn(false)}>logout</li></>: publicRouteData.map(({name,path,id})=> <li key={id}><Link to={path}>{name}</Link></li>)
                    }
                </ul>
            </nav>
            </header>
    )
}

export default Navigation
