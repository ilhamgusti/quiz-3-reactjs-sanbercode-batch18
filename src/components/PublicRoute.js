import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { AuthContext } from '../contexts/AuthContext';

const PublicRoute = ({ component: Component, restricted, ...rest }) => {
    const { isLoggedIn } = useContext(AuthContext);
    return (
        <Route
            {...rest}
            render={(props) =>
                isLoggedIn && restricted ? (
                    <Redirect to="/" />
                ) : (
                        <Component {...props} />
                    )
            }
        />
    )
};

export default PublicRoute;
