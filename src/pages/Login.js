import React, { useContext, useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import { Section } from '../components'
import { AuthContext } from '../contexts'

const Login = () => {
    const { handleSubmitLogin, input, handleInput, isLoggedIn } = useContext(AuthContext);
    const history = useHistory();
    useEffect(() => {
        if (isLoggedIn) {
            history.push('/movie-list-editor')
        } else {
            history.push('/login')
        }
    }, [history, isLoggedIn])
    return (
        <Section title="Login">
            <form className="form-edit-movie" style={{ padding: '5rem' }} onSubmit={handleSubmitLogin}>
                <label htmlFor="username">username</label>
                <input type="text" name="username" id="username" onChange={handleInput} value={input.username}/>
                <label htmlFor="password">password</label>
                <input type="text" name="password" id="password" onChange={handleInput} value={input.password}/>
                <button>Login</button>
            </form>
        </Section>
    )
}

export default Login
