import React, { Component } from 'react'
import { Section } from '../components'
import { MovieContext } from '../contexts'


export default class Home extends Component {
    static contextType = MovieContext;
    render() {
        const { data } = this.context;
        return (
            <Section title="Daftar Film Pendek Terbaik">
                {data.map((movie)=>{
                    return <React.Fragment key= {movie.id}>
                            <div className="movie-grid-container">
                                
                            <h3 className="movie-title">{movie.title}</h3>
                            <img className="movie-image" src={movie.image_url} alt={movie.title} width="200" />
                            <div className="movie-detail">
                                <div><b>Rating:</b> {movie.rating}</div>
                                <div><b>Year:</b> {movie.year}</div>
                                <div><b>Duration:</b> {movie.duration}</div>
                                <div><b>Genre:</b> {movie.genre}</div>
                            </div>
                                <p className="movie-description">Description: {movie.description}</p>
                            </div>
                        </React.Fragment>
                })}
                </Section>
        )
  }
}