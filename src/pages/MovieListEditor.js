import React, { useContext, useEffect } from 'react';
import { Section } from '../components';
import { MovieContext } from '../contexts';
import { truncate } from '../utils';

const filterCondition = title => title !== 'created_at' && title !== 'updated_at' && title !== 'id' && title !== 'image_url' && title !== 'review'

const MovieListEditor = () => {
    const { data, handleInput, handleDelete, input, handleEdit, handleSubmit } = useContext(MovieContext);
    
    useEffect(() => {
        console.log(data);
    }, [data])
    console.log({dataFromXX:data});
    return (
        <Section title="Movie List Editor">
            <Section title="Daftar Film">
                <table className="table-movie">
                    <thead>
                        <tr>
                            {data.length && Object.keys(data[0]).filter(filterCondition).map(title => <th key={title}>{title}</th>)}
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data.length && data.map(movie => {
                                return <tr key={movie.id}>
                                    <td>{movie.title}</td>
                                    <td>{truncate(movie.description, 30)}</td>
                                    <td>{movie.year}</td>
                                    <td>{movie.duration}</td>
                                    <td>{movie.genre}</td>
                                    <td>{movie.rating}</td>
                                    <td>
                                        <button onClick={handleEdit} value={movie.id}>edit</button>
                                        <button onClick={handleDelete} value={movie.id}>delete</button>
                                    </td>
                                </tr>
                            })
                        }
                    </tbody>
                </table>
            </Section>
            <Section title="Form Edit Film">
                <form onSubmit={handleSubmit} className="form-edit-movie">
                    <label htmlFor="title">Title</label>
                    <input onChange={handleInput} type="text" name="title" id="title" value={input.title} />
                    <label htmlFor="description">Description</label>
                    <textarea onChange={handleInput} name="description" id="description" value={input.description}/>
                    <label htmlFor="year">year</label>
                    <input onChange={handleInput} type="number" name="year" id="year"  min={1980} value={input.year}/>
                    <label htmlFor="duration">duration</label>
                    <input onChange={handleInput} type="number" name="duration" id="duration"  value={input.duration}/>
                    <label htmlFor="genre">genre</label>
                    <input onChange={handleInput} type="text" name="genre" id="genre" value={input.genre} />
                    <label htmlFor="rating">rating</label>
                    <input onChange={handleInput} type="number" name="rating" id="rating" min={0} max={10} value={input.rating} />
                    <label htmlFor="image_url">Image Url</label>
                    <input onChange={handleInput} type="text" name="image_url" id="image_url" value={input.image_url} />

                    <button>Submit</button>
                </form>
            </Section>
        </Section>
    )
}

export default MovieListEditor
