import React from 'react'
import { Section } from '../components'

const About = () => {
    return (
        <Section title="About">
            <ol>
                <li><strong style={{width:'100%'}}>Nama:</strong> Ilham Gusti Wibowo</li> 
                <li><strong style={{width:'100%'}}>Email:</strong> <a href="mailto:ilham.wibowo51@gmail.com">ilham.wibowo51@gmail.com</a></li> 
                <li><strong style={{width:'100%'}}>Sistem Operasi yang digunakan:</strong> Windows 10</li>
                <li><strong style={{width:'100%'}}>Akun Gitlab:</strong> <a href="https://gitlab.com/ilhamgusti">ilhamgusti</a></li> 
                <li><strong style={{width:'100%'}}>Akun Telegram:</strong> <a href="https://t.me/ilhmgst">@ilhmgst</a></li> 
            </ol>
        </Section>
    )
}

export default About
