import { Home, About, Login, MovieListEditor  } from '../pages';

export const publicRouteData = [
    {
        id: 1,
        name: 'Home',
        path:'/',
        component: Home
    },
    {
        id: 2,
        name: 'About',
        path:'/about',
        component: About
    },
    {
        id: 3,
        name: 'Login',
        path:'/login',
        component: Login
    },
];

export const privateRouteData = [
    {
        id: 1,
        name: 'Home',
        path:'/',
        component: Home
    },
    {
        id: 2,
        name: 'About',
        path:'/about',
        component: About
    },
    {
        id: 3,
        name: "Movie List Editor",
        path: '/movie-list-editor',
        component: MovieListEditor
    }
];