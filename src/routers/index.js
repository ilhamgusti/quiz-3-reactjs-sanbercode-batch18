import React from 'react';
import { BrowserRouter as ILRouter, Route, Switch } from 'react-router-dom';
import { Footer, Navigation} from '../components';
import PrivateRoute from '../components/PrivateRoute';
import PublicRoute from '../components/PublicRoute';
import { privateRouteData, publicRouteData } from './routeData';

const router = (
  <ILRouter>
    <Navigation/>
    <Switch>
      {publicRouteData.map(({ id, ...dataRoute }) => <PublicRoute key={id} exact {...dataRoute} />)}
      {privateRouteData.map(({id,...dataRoute})=> <PrivateRoute key={id} exact {...dataRoute}/>)}
    </Switch>
    <Footer/>
  </ILRouter>
);

export default router;
